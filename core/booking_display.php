<?php
    $order = new jbOrderList();
    $service = new jbServiceList();
    $apartment = new jbApartment();
    $category = new jbCategory();
    $user = new jbUser();

    if (!empty($_POST[$order->name_post]) && !empty($_POST[$user->name_post])) {
        if ($order->addOrder() && $user->add($order->getId()))
            $jb_message = 'New order created.';
    }

    $category_bb = $category->getCategoryById(1)->name;
    $category_self = $category->getCategoryById(2)->name;

    $service_list_bb = $service->getServiceByCategory($category_bb);
    $service_list_self = $service->getServiceByCategory($category_self);

    $apartment_list_bb = $apartment->getApartamentByCategory($category_bb);
    $apartment_list_self = $apartment->getApartamentByCategory($category_self);

    //var_dump($_POST);
?>
    
    <script>    
        
    var days = sum = pets = 0,        
        curCost = <?= $service_list_bb[0]->coast?>,       
        info = <?= ($order->getDate()) ?>,
        type = '<?= $category_self ?>',
        apartment = <?= json_encode($apartment_list_self[0]->name) ?>,
        checkInDate,
        checkOutDate;
    var unavailableDates = [];

        function countSum(cost){
            curCost = cost;
            if(days < 7){
                sum = cost*days + pets;
            }
            else {               
                sum = cost*days + pets * Math.round(days/7);
            }            
           
            document.getElementById('sum').value = sum; 
            //document.getElementById('total').innerHTML = sum;           
            if(type == '<?= $category_self ?>'){
                if(days == 3){
                    document.getElementById(<?= json_encode($service_list_self[0]->id)?>).value = <?= json_encode($service_list_self[0]->name)?>;
                    document.getElementById(<?= json_encode($service_list_self[1]->id)?>).value = "";
                    document.getElementById(<?= json_encode($service_list_self[2]->id)?>).value = "";
                }
                if(days == 4){
                    document.getElementById(<?= json_encode($service_list_self[0]->id)?>).value = "";
                    document.getElementById(<?= json_encode($service_list_self[1]->id)?>).value = <?= json_encode($service_list_self[1]->name)?>;
                    document.getElementById(<?= json_encode($service_list_self[2]->id)?>).value = "";
                }
                if(days >= 7){
                    document.getElementById(<?= json_encode($service_list_self[0]->id)?>).value = "";
                    document.getElementById(<?= json_encode($service_list_self[1]->id)?>).value = "";
                    document.getElementById(<?= json_encode($service_list_self[2]->id)?>).value = <?= json_encode($service_list_self[2]->name)?>;
                }                
            }
            if(type == '<?= $category_bb ?>'){   
                
                if(cost.toString() == <?= json_encode($service_list_bb[0]->coast)?>)  {                     
                    document.getElementById(<?= json_encode($service_list_bb[0]->id)?>).value = <?= json_encode($service_list_bb[2]->name)?>;
                    document.getElementById(<?= json_encode($service_list_bb[1]->id)?>).value = "";
                    document.getElementById(<?= json_encode($service_list_bb[2]->id)?>).value = "";
                } 
                if(cost.toString() == <?= json_encode($service_list_bb[1]->coast)?>)  {                     
                    document.getElementById(<?= json_encode($service_list_bb[0]->id)?>).value = "";
                    document.getElementById(<?= json_encode($service_list_bb[1]->id)?>).value = <?= json_encode($service_list_bb[2]->name)?>;
                    document.getElementById(<?= json_encode($service_list_bb[2]->id)?>).value = "";
                }    
                if(cost.toString() == <?= json_encode($service_list_bb[2]->coast)?>)  {                     
                    document.getElementById(<?= json_encode($service_list_bb[0]->id)?>).value = "";
                    document.getElementById(<?= json_encode($service_list_bb[1]->id)?>).value = "";
                    document.getElementById(<?= json_encode($service_list_bb[2]->id)?>).value = <?= json_encode($service_list_bb[2]->name)?>;
                }                
            }
        }

        jQuery(function($) {

            <?php foreach ($apartment_list_self as $key => $value): ?> 
                $('#apartmentSelect').append('<option>' + '<?= $value->name; ?>' + '</option>');              
            <?php endforeach; ?> 
                       
            
            function daysDisable(date, days){
                var dmy = dateParse(date);
                if(days === undefined) { days = [0,1,2,3,4,5,6]; }
                if(info[type] === undefined){                    
                    return [true && $.inArray(date.getDay(), days) != -1, ""]
                }
                else{
                    
                    if ($.inArray(dmy, getUnavaliableDates(info[type][apartment])) != -1) {
                                      
                        return [false, ""]; 
                    }
                    else{    
                                           
                        return [true && $.inArray(date.getDay(), days) != -1, ""];                        
                    } 
                }                   
            }               

            function getUnavaliableDates(arr){                
                var day = 1000*60*60*24;                    
                if(arr){                        
                    for(var i = 0; i < arr.length; i++){ 
                        dates = arr[i];                                                
                        var start = new Date(dates.start);
                        var finish = new Date(dates.finish);
                        var diff = (finish.getTime() - start.getTime())/day;

                        for(var j=0; j<=diff; j++)
                        {
                           var xx = dateParse(start.getTime()+day*j);                       
                            unavailableDates.push(xx);                      
                        }   
                    }
                    return unavailableDates;
                }
                else return [];
            }                            
            
            function dateParse(date){
                var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                return [year, month, day].join('-');
            }


            $(document).ready(function(){
                
                $('#showForm').click(function(){
                    $('#clientForm').show();
                    $(this).hide();
                });

                $('#self').click();                
                
                $( "#datepicker" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: new Date(),           
                    beforeShowDay: function(d) {
                        return daysDisable(d, [1,4,5]);
                    },
                     onSelect: function(selectedDate)
                    { 
                        var d = new Date (selectedDate);
                        $("#datepicker2").datepicker("option", "minDate", dateParse(d.setDate(d.getDate() + 1)));
                        console.log(d);
                        checkInDate = selectedDate;
                        if (checkOutDate){
                            days = (new Date(checkOutDate) - new Date(checkInDate))/(1000*60*60*24);
                            countSum(curCost);                            
                        }
                        else {
                            //console.log(unavailableDates);
                            for (var i = 0; i < unavailableDates.length; i++){                                 
                                 if( new Date(selectedDate) < new Date(unavailableDates[i])){
                                     $("#datepicker2").datepicker("option", "maxDate", unavailableDates[i]);
                                     break;
                                }
                            }  
                        } 
                    }                    
                }); 

                $( "#datepicker2" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: new Date(),
                    beforeShowDay: function(d) {
                        return daysDisable(d, [1]);
                    },
                    onSelect: function(selectedDate)
                    { 
                        var d = new Date (selectedDate);
                        $("#datepicker").datepicker("option", "maxDate", dateParse(d.setDate(d.getDate() - 1)));                  
                        checkOutDate = selectedDate;
                        if (checkInDate){                            
                            days = (new Date(checkOutDate) - new Date(checkInDate))/(1000*60*60*24);                            
                            countSum(curCost);                            
                        }
                        else {
                            for (var i = 0; i < unavailableDates.length; i++){                                 
                                 if( new Date(selectedDate) > new Date(unavailableDates[i])){
                                    console.log('lol');
                                     $("#datepicker").datepicker("option", "minDate", unavailableDates[i]);
                                     break;
                                }
                            }  
                        }                      
                    }           
                });
                
                $('#self').change(function(){

                    $('.bbShow').hide();
                    $('.selfShow').show();

                    type = '<?= $category_self ?>';

                    checkInDate = checkOutDate = days = 0;

                    $('#bookType').val(type);

                    $('#apartmentSelect').children().remove();

                    <?php foreach ($apartment_list_self as $key => $value): ?> 
                        $('#apartmentSelect').append('<option>' + '<?= $value->name; ?>' + '</option>');              
                    <?php endforeach; ?> 

                    $('#cost').children().remove();

                    <?php foreach ($service_list_self as $key => $value): ?>
                        $('#cost').append(`<span><?= $value->name; ?> - &pound;<?= $value->coast; ?></span><br>
                        <input type="hidden" name="order_add[self]" id="<?= $value->id;?>"> `);         
                    <?php endforeach; ?>
                   
                   $('#datepicker').datepicker("option", "beforeShowDay", function(d){ 
                        return daysDisable(d, [1, 4, 5]);                       
                    }).datepicker('setDate', null);

                    $('#datepicker2').datepicker("option", "beforeShowDay", function(d){ 
                        return daysDisable(d, [1]);                        
                    }).datepicker('setDate', null);

                    countSum(curCost);

                    apartment = $('#apartmentSelect').val();
                                       
                });                

                $('#bnb').change(function(){

                    $('.selfShow').hide();
                    $('.bbShow').show();

                    type = '<?= $category_bb?>';
                    
                    checkInDate = checkOutDate = days = 0;

                    $('#bookType').val(type); 

                    $('#apartmentSelect').children().remove();

                    <?php foreach ($apartment_list_bb as $key => $value): ?>
                        $('#apartmentSelect').append('<option>' + '<?= $value->name; ?>' + '</option>');              
                    <?php endforeach; ?> 

                    $('#cost').children().remove();    

                    <?php foreach ($service_list_bb as $key => $value): ?>
                        $('#cost').append(`<input type="radio" name="unit" id="<?= $value->id;?>radio" onchange="countSum(<?= $value->coast; ?>)">
                        <label for="<?= $value->id;?>radio"><?= $value->name; ?> - &pound;<?= $value->coast; ?></label><br>
                        <input type="hidden" name="order_add[self]" id="<?= $value->id;?>">`);
                    <?php endforeach; ?> 

                    $('#1radio').attr('checked', 'checked');             
                                       
                    $('#datepicker').datepicker("option", "beforeShowDay", function(d){ 
                        return daysDisable(d);
                    }).datepicker('setDate', null).datepicker("option", "maxDate", null);
                    
                    $('#datepicker2').datepicker("option", "beforeShowDay", function(d){ 
                        return daysDisable(d);
                    }).datepicker('setDate', null).datepicker("option", "minDate", new Date());
                    
                    countSum(curCost);

                    apartment = $('#apartmentSelect').val(); 
                                      
                });

                $('#apartmentSelect').change(function(){
                    apartment = $(this).val();                    
                    checkInDate = checkOutDate = days = 0;

                    $('#datepicker').datepicker('setDate', null); 
                    $('#datepicker2').datepicker('setDate', null);

                    countSum(curCost);              
                });

                
                $('#pets').change(function(){
                    if (typeof $(this).attr('checked') !== typeof undefined && $(this).attr('checked') !== false) {

                        if(type == '<?=$category_bb?>'){
                            pets = 15;
                        }
                        if (type == '<?= $category_self ?>'){
                            if(days < 7){
                                pets = 35;
                            }
                            else{
                                pets = 45;
                            }
                        }
                    }
                    else{
                        pets = 0;
                    }
                    countSum(curCost);
                });
              
            });            

        });       
       
    </script>
    <div class="panel">
        <div class="panel-heading" style="text-align: center;">
            
        </div>
        <div class="panel-body">
            <div><?= $jb_message ?></div>
            <div class="full-width row type">
                <h2>Choose Something</h2>
                <div class="col-md-6">
                    <input type="radio" name="type" id="self" checked ><label for="self">Self Catering</label>
                </div>
                <div class="col-md-6">            
                    <input type="radio" name="type" id="bnb"><label for="bnb">Bed & Breakfast</label>
                </div>
            </div>
            <form action="" method="post" name="form">     

                <input type="hidden" name="order_add[service]" value="<?= $category_self ?>" id="bookType">
                <div class="full-width row">
                    <h2>Choose Apartment</h2>
                    <select name="order_add[apartment]" id="apartmentSelect">                
                    </select>
                </div>
                
                <div class="full-width row">
                    <h2>Choose chek-in/check-out dates</h2>
                    <div class="datepicker_div col-md-6">Check-in: <br>
                        <input name="order_add[check_in]" type="text" id="datepicker" placeholder="Check-in date" value="">
                    </div>
                    <div class="datepicker_div col-md-6">Check-out: <br>
                        <input name="order_add[check_out]" type="text" id="datepicker2" placeholder="Check-in date" value="">
                    </div>
                </div>
                <div class="full-width info">
                    <div id="cost" class="full-width left">
                        <?php foreach ($service_list_self as $key => $value): ?>
                            <span><?= $value->name; ?> - &pound;<?= $value->coast; ?></span><br>
                            <input type="hidden" name="order_add[self]" id="<?= $value->id;?>">          
                        <?php endforeach; ?>            
                    </div>      
                   
                     <div class="bbShow full-width left" style="display: none;">
                        <span>Pets - &pound;15 per stay</span><br> 
                    </div>
                    <div class="selfShow full-width left">
                        <span>Pets - &pound;35 per part week</span><br>
                        <span>Pets - &pound;45 per full week</span><br>  
                    </div>
                </div>
                <div class="full-width left">
                    <input name="order_add[pets]" type="checkbox" id="pets" value="1">
                    <label for="pets">Pets</label><br>
                </div>
               
                <div id="total" class="full-width left">
                    <label class="col-md-3" for="sum">Total cost (&pound;): </label>
                    <input class="col-md-8" name="order_add[coast]" type="text" id="sum" value="0" disabled>
                    
                </div>
                <input name="order_add[status]" type="hidden" id="status" value="1">
                <div class="row" style="display: none;" id="clientForm">
                    <h2>Fill all fields</h2>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="firstName">First name:</label>
                        <input  name="user_add[first_name]" type="text" class="form-control col-md-6" id="firstName" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="lastName">Last name:</label>
                        <input name="user_add[last_name]" type="text" class="form-control col-md-6" id="lastName" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="email">Email:</label>
                        <input name="user_add[email]" type="text" class="form-control col-md-6" id="email" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="phone">Phone number:</label>
                        <input name="user_add[phone_number]" type="text" class="form-control col-md-6" id="phone" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="mobile">Mobile number:</label>
                        <input name="user_add[mobile_number]" type="text" class="form-control col-md-6" id="mobile" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="addr">Address:</label>
                        <input name="user_add[adress]" type="text" class="form-control col-md-6" id="addr" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="city">City:</label>
                        <input name="user_add[city]" type="text" class="form-control col-md-6" id="city" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="state">State:</label>
                        <input name="user_add[state]" type="text" class="form-control col-md-6" id="state" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="code">Postal code:</label>
                        <input name="user_add[post]" type="text" class="form-control col-md-6" id="code" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-md-3" for="country">Country:</label>
                        <input name="user_add[country]" type="text" class="form-control col-md-6" id="country" required>
                    </div>
                    <div class="col-md-12 submit">
                        <input type="submit" class="btn">
                    </div>
                    
                </div>
                <div class="col-md-12 submit">        
                    <button class="btn" id="showForm">Order</button>
                </div>

            </form>
        </div>
    </div>
    
    
    

