<?php

    function jal_install () {
        global $wpdb;

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $table_name = $wpdb->prefix . "jb_user";
        if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
         
            $sql = "CREATE TABLE " . $table_name . " (
                id                      mediumint(9) NOT NULL AUTO_INCREMENT,
                id_order                mediumint(9),
                first_name              VARCHAR(255) NOT NULL,
                last_name               VARCHAR(255) NOT NULL,
                email                   VARCHAR(255) NOT NULL,
                phone_number            VARCHAR(255) NOT NULL,
                mobile_number           VARCHAR(255) NOT NULL,
                adress                  VARCHAR(255) NOT NULL,
                city                    VARCHAR(255) NOT NULL,
                state                   VARCHAR(255) NOT NULL,
                post                    VARCHAR(255) NOT NULL,
                country                 VARCHAR(255) NOT NULL,
                UNIQUE KEY id (id)
              );";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }

        $table_name = $wpdb->prefix . "jb_order";
        if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
             
            $sql = "CREATE TABLE " . $table_name . " (
                id                      mediumint(9)    NOT NULL AUTO_INCREMENT,
                service                 VARCHAR(255)    NOT NULL,
                self                    VARCHAR(255)    NOT NULL,
                check_in                DATE            NOT NULL,
                check_out               DATE            NOT NULL,
                pets                    BOOLEAN         DEFAULT 0,
                coast                   DOUBLE          NOT NULL,
                status                  BOOLEAN         NOT NULL,
                UNIQUE KEY id (id)
              );";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }

        $wpdb->query('ALTER TABLE `'.$table_name.'` ADD `apartment` VARCHAR(255) NOT NULL AFTER `self`');
        $wpdb->query('ALTER TABLE `'.$table_name.'` AUTO_INCREMENT=100');

        $table_name = $wpdb->prefix . "jb_setting";
        if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
             
            $sql = "CREATE TABLE " . $table_name . " (
                id                      mediumint(9) NOT NULL AUTO_INCREMENT,
                name                    VARCHAR(255) NOT NULL,
                value                   TEXT,
                UNIQUE KEY id (id)
              );";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }       

        $table_name = $wpdb->prefix . "jb_category";
        if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
             
            $sql = "CREATE TABLE " . $table_name . " (
                id                      mediumint(9) NOT NULL AUTO_INCREMENT,
                name                    VARCHAR(255) NOT NULL,
                active                  int NOT NULL,
                UNIQUE KEY id (id)
              );";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            dbDelta($sql);
            $sql = 'INSERT INTO `'.$table_name.'` (`name`, `active`) VALUES ("Bed & Breakfast", 1)';
            dbDelta($sql);
            $sql = 'INSERT INTO `'.$table_name.'` (`name`, `active`) VALUES ("Self catering", 1)';
            dbDelta($sql);
        }

        $table_name = $wpdb->prefix . "jb_service";
        if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
             
            $sql = "CREATE TABLE " . $table_name . " (
                id                      mediumint(9) NOT NULL AUTO_INCREMENT,
                name                    VARCHAR(255) NOT NULL,
                category                VARCHAR(255) NOT NULL,
                coast                   DOUBLE      NOT NULL,
                active                  INT         NOT NULL,
                UNIQUE KEY id (id)
              );";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
            $service = new jbServiceList();
            for ($i=0; $i<count($service->default_service_B); $i++)
            {
                $sql = 'INSERT INTO `'.$table_name.'` (`name`, `category`, `coast`, `active`) VALUES ("
                    '.$service->default_service_B[$i]['name'].'", "Bed & Breakfast", '.$service->default_service_B[$i]['coast'].', 1)';
                dbDelta($sql);

            }
            for ($i=0; $i<count($service->default_service_S); $i++)
            {
                $sql = 'INSERT INTO `'.$table_name.'` (`name`, `category`, `coast`, `active`) VALUES ("
                    '.$service->default_service_S[$i]['name'].'", "Self catering", '.$service->default_service_S[$i]['coast'].', 1)';
                dbDelta($sql);

            }  
                
            } 

        $table_name = $wpdb->prefix . "jb_apartment";
        if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
             
            $sql = "CREATE TABLE " . $table_name . " (
                id                      mediumint(9) NOT NULL AUTO_INCREMENT,
                name                    VARCHAR(255) NOT NULL,
                category                VARCHAR(255) NOT NULL,
                active                  INT         NOT NULL,
                UNIQUE KEY id (id)
              );";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
            $self_catering = array('Granary Cottage',
                                    'The Nest',
                                    'Hazel Cottage',
                                    'Stable Cottage',
                                    'Kyte Lodge',
                                    'Kestrel Lodge',
                                    'The Studio');
            foreach ($self_catering as $value)
            {
                $sql = 'INSERT INTO `'.$table_name.'` (`name`, `category`, `active`) VALUES ("'.$value.'", "Self catering", 1)';
                dbDelta($sql);             
            }

            for ($i=1; $i<=10; $i++)
            { 
                if($i!=10)
                {
                    $sql = 'INSERT INTO `'.$table_name.'` (`name`, `category`, `active`) VALUES ("'.$i.'", "Bed & Breakfast", 1)';
                    dbDelta($sql); 
                }
                else
                {
                    $sql = 'INSERT INTO `'.$table_name.'` (`name`, `category`, `active`) VALUES ("'.$i.'", "Bed & Breakfast", 0)';
                    dbDelta($sql);
                }

            }
                
            }

   }

   register_activation_hook($file,'jal_install');