<?php
$service = new jbServiceList();

$category = new jbCategory();


$category_list = $category->getCategory('enable');


$submit = 'Add new service';

$name_service = '';

$coast = '';
$checking = 1;
$category = $category_list[0]->name;


if($_GET['id'])
{
	$info_service = $service->getServiceById($_GET['id']);

	$submit = 'Update service';

	$name_service = $info_service->name;

	$coast = $info_service->coast;

	$checking = $info_service->active;

}
?>

<div class="wrap">
	<h1 id="add-new-user">Add new services</h1>
	<div id="ajax-response"></div>
	<p>Create a new service and add booking system.</p>
	<form  action="?page=booking-service" method="post">
		<table class="form-table">
			<tbody>
				<tr class="form-field form-required">
					<th scope="row"><label for="name_service">Name service <span class="description">(required)</span></label></th>
					<td>
						<input name="name_service" type="text" id="name_service" value="<?=$name_service?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
						<p class="description" id="tagline-description">Name of service Booking System</p>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row">
						<label for="cost">Cost </label>
					</th>
					<td>
						<input name="coast" type="text" id="coast" value="<?=$coast?>">
						<p class="description" id="tagline-description">The cost of this service</p>
					</td>
				</tr>		
				<tr class="form-field">
					<th scope="row">
						<label for="Category">Category</label>
					</th>
					<td>
						<select name="category" id="category">
							<?php foreach ($category_list as $key => $value): ?>
								<option value="<?=$value->name?>"><?=$value->name?></option>
							<?php endforeach; ?>	
						</select>
						<p class="description" id="tagline-description">Select a category for this service</p>
					</td>
				</tr>
				<tr>
					<th scope="row">Service active</th>
					<td>
						<input type="checkbox" name="active" id="active" value="1"
							<?php $check = ($checking == 1) ? 'checked' : ''?>; 
							<?=$check?>>
						<p class="description" id="tagline-description">Enable or Disable Service</p></td>
				</tr>
			</tbody>
		</table>
		<div class="submit" style="clear:both;">
			<input type="submit" name="add_service" id="createusersub" class="button button-primary" value="<?=$submit?>">
			<input type="hidden" name="id" value="<?=$_GET['id']?>">
		</div>
	</form>
</div>