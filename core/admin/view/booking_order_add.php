<?php 

$status = $order->getStatus();

$accomodation_list = $accomodation->getCategory();

$service_list = $service->getService();

$apparment_list = $appartment->getApartamentByCategory(($order_data->appartment ? $order_data->appartment : $accomodation_list[0]->name));

$now_service_list = $service->getServiceByCategory(($order_data->service ? $order_data->service : $accomodation_list[0]->name));

?>
<div class="wrap">
	<h1>Add order</h1>
	<p>Add new order to the booking system.</p>
	<form action="?page=booking" method="post">
		<input type="hidden" name="id" value="<?= $order_data->id; ?>" />
		<input type="hidden" name="action_input" value="<?= $action ?>" />
		<input type="hidden" name="order_add[coast]" value="743.00" />
		<table class="form-table">
			<tbody>
				<tr class="form-field">
					<th scope="row"><label for="user_login">Accommodation</label></th>
					<td>
						<select name="order_add[service]">
							<?php foreach ($accomodation_list as $key => $value): ?>
								<option <?= ($value->name == $order_data->service ? 'selected="selected"' : '') ?> value="<?= $value->name; ?>"><?= $value->name; ?></option>
							<?php endforeach; ?>	
						</select>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row"><label for="user_login">Appartment</label></th>
					<td>
						<select name="order_add[apartment]">
							<?php foreach ($apparment_list as $key => $value): ?>
								<option <?= ($value->name == $order_data->apartment ? 'selected="selected"' : '') ?> value="<?= $value->name; ?>"><?= $value->name; ?></option>
							<?php endforeach; ?>	
						</select>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row"><label for="user_login">Service</label></th>
					<td>
						<select name="order_add[self]">
							<?php foreach ($now_service_list as $key => $value): ?>
								<option data-cost="<?= $value->coast; ?>" <?= ($value->name == $order_data->self ? 'selected="selected"' : '') ?> value="<?= $value->name; ?>"><?= $value->name; ?></option>
							<?php endforeach; ?>	
						</select>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row"><label for="input_check_in">Check in <span class="description">(required)</span></label></th>
					<td><input id="input_check_in" name="order_add[check_in]" type="text" class="regular-text code" value="<?= $order_data->check_in; ?>"></td>
				</tr>
				<tr class="form-field">
					<th scope="row"><label for="input_check_out">Check out <span class="description">(required)</span></label></th>
					<td><input id="input_check_out" name="order_add[check_out]" type="text" class="regular-text code" value="<?= $order_data->check_out; ?>"></td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label for="user_login">Pets</label></th>
					<td>
						<label>
							<input type="checkbox" name="order_add[pets]" value="1" <?= ($order_data->pets ? 'checked' : '') ?> >
						</label>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row"><label for="user_login">Status</label></th>
					<td>
						<select name="order_add[status]">
							<?php foreach ($status as $key => $value): ?>
								<option <?= ($key == $order_data->status ? 'selected="selected"' : '') ?> value="<?= $key; ?>"><?= $value['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row">Cost</th>
					<td>
						<big><b>745.00 &#163;</b></big>
					</td>
				</tr>
			</tbody>
		</table>
		<h1>Info about customer</h1>
		<table class="form-table">
			<tbody>
				<tr class="form-field">
					<th><label for="">First name <span class="description">(required)</span></label></th>
					<td><input  name="user_add[first_name]" type="text" class="regular-text code" value="<?= $user_data->first_name; ?>"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Last name <span class="description">(required)</span></label></th>
					<td><input name="user_add[last_name]" type="text" class="regular-text code" value="<?= $user_data->last_name; ?>"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Email <span class="description">(required)</span></label></th>
					<td><input name="user_add[email]" type="text" class="regular-text code" value="<?= $user_data->email; ?>"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Phone number <span class="description">(required)</span></label></th>
					<td><input name="user_add[phone_number]" type="text" class="regular-text code" value="<?= $user_data->phone_number; ?>"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Mobile number <span class="description">(required)</span></label></th>
					<td><input name="user_add[mobile_number]" type="text" class="regular-text code" value="<?= $user_data->mobile_number; ?>"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Adress <span class="description">(required)</span></label></th>
					<td><input name="user_add[adress]" type="text" class="regular-text code" value="<?= $user_data->adress; ?>"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">City <span class="description">(required)</span></label></th>
					<td><input name="user_add[city]" type="text" class="regular-text code" value="<?= $user_data->city; ?>"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">State <span class="description">(required)</span></label></th>
					<td><input name="user_add[state]" type="text" class="regular-text code" value="<?= $user_data->state; ?>"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Post code <span class="description">(required)</span></label></th>
					<td><input name="user_add[post]" type="text" class="regular-text code" value="<?= $user_data->post; ?>"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Country <span class="description">(required)</span></label></th>
					<td><input name="user_add[country]" type="text" class="regular-text code" value="<?= $user_data->country; ?>"></td>
				</tr>
			</tbody>
		</table>
		<div class="submit" style="clear:both;">
			<input type="submit" name="submit" id="createusersub" class="button button-primary" value="<?= $action ?> order">
		</div>
	</form>
</div>
