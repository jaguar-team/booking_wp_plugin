<?php
	if ($_GET['field'] && $_GET['order'])
		$order_by = ['field' => $_GET['field'], 'value' => $_GET['order']];
	if ($_GET['status'])
		$where = ['field' => 'status', 'value' => $_GET['status']];
	
	$order_list = $order->getSortOrder($where, $order_by);


	$fields = $order->fields;
	$status = $order->getStatus();
	$pets 	= $order->getPets();

?>
<?php if ($order_list): ?>
	<div class="wrap">
		<h1>
			Order list
			<a href="?page=booking&action=add" class="page-title-action">Add order</a>
		</h1>
		<?php if ($jb_message): ?>
			<div id="message" class="updated notice is-dismissible">
					<p><?= $jb_message; ?></p>
			</div>
		<?php endif; ?>
		<ul class="subsubsub">
			<li class="all"><a <?= ($order->checkActiveLink('status') ? 'class="current"' : ''); ?> href="<?= $order->getLink(); ?>">All</a> <span class="count">(<?= $order->getSortOrder(false, false, true); ?>)</span></li>
			<li class="administrator"><a <?= ($order->checkActiveLink('status', 1) ? 'class="current"' : ''); ?> href="<?= $order->getLink('status', 1); ?>">Confirmed <span class="count">(<?= $order->getSortOrder(['field' => 'status', 'value' => 1], false, true); ?>)</span></a></li>
			<li class="administrator"><a <?= ($order->checkActiveLink('status', 2) ? 'class="current"' : ''); ?> href="<?= $order->getLink('status', 2); ?>">Not confirmed <span class="count">(<?= $order->getSortOrder(['field' => 'status', 'value' => 2], false, true); ?>)</span></a></li>
		</ul>
		<form method="post" action="">
			<div class="tablenav top">
				<div class="alignleft actions bulkactions">
					<select name="action" id="bulk-action-selector-top">
							<option value="0">Action</option>
							<option value="delete">Delete</option>
					</select>
					<input type="submit" id="doaction" class="button action" value="Apply">
				</div>
				<br class="clear">
			</div>
			<table class="wp-list-table widefat fixed striped users">
				<thead>
					<tr>
						<td id="cb" class="manage-column column-cb check-column">
							<label class="screen-reader-text" for="cb-select-all-1">Выделить все</label>
							<input id="cb-select-all-1" type="checkbox">
						</td>
						<th class="sortable <?= $order->getCurrentSort('id'); ?>">
							<a href="<?= $order->getLink('field', 'id', true); ?>"><span>ID</span> <span class="sorting-indicator"></span></a>
						</th>
						<th>Accommodation</th>
						<th>Appartment</th>
						<th>Service</th>
						<th class="sortable <?= $order->getCurrentSort('check_in'); ?>">
							<a href="<?= $order->getLink('field', 'check_in', true); ?>"><span>Check in</span> <span class="sorting-indicator"></span></a>
						</th>
						<th class="sortable <?= $order->getCurrentSort('check_out'); ?>">
							<a href="<?= $order->getLink('field', 'check_out', true); ?>"><span>Check out</span> <span class="sorting-indicator"></span></a>
						</th>
						<th>Pets</th>
						<th class="sortable <?= $order->getCurrentSort('coast'); ?>">
							<a href="<?= $order->getLink('field', 'coast', true); ?>"><span>Coast</span> <span class="sorting-indicator"></span></a>
						</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($order_list as $key => $value): ?>
						<tr>
							<th scope="row" class="check-column">
								<label class="screen-reader-text" for=""></label>
								<input type="checkbox" name="order[]" value="<?= $value->id; ?>">
							</th>
							<td>
								<a href="?id=<?= $value->id; ?>&page=booking&action=update"><?= $value->id; ?></a>
							</td>
							<td>
								<?= $value->service; ?>
							</td>
							<td>
								<?= $value->apartment; ?>
							</td>
							<td>
								<?= $value->self; ?>
							</td>
							<td>
								<?= $value->check_in; ?>
							</td>
							<td>
								<?= $value->check_out; ?>
							</td>
							<td>
								<?= $pets[$value->pets]['name']; ?>
							</td>
							<td>
								<?= number_format((float)$value->coast, 2, '.', '').' &#163'; ?>
							</td>
							<td style="color:<?= $status[$value->status]['color'] ?>">
								<?= $status[$value->status]['name']; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<td id="cb" class="manage-column column-cb check-column">
							<label class="screen-reader-text" for="cb-select-all-1">Выделить все</label>
							<input id="cb-select-all-1" type="checkbox">
						</td>
						<th class="sortable <?= $order->getCurrentSort('id'); ?>">
							<a href="<?= $order->getLink('field', 'id', true); ?>"><span>ID</span> <span class="sorting-indicator"></span></a>
						</th>
						<th>Accommodation</th>
						<th>Appartment</th>
						<th>Service</th>
						<th class="sortable <?= $order->getCurrentSort('check_in'); ?>">
							<a href="<?= $order->getLink('field', 'check_in', true); ?>"><span>Check in</span> <span class="sorting-indicator"></span></a>
						</th>
						<th class="sortable <?= $order->getCurrentSort('check_out'); ?>">
							<a href="<?= $order->getLink('field', 'check_out', true); ?>"><span>Check out</span> <span class="sorting-indicator"></span></a>
						</th>
						<th>Pets</th>
						<th class="sortable <?= $order->getCurrentSort('coast'); ?>">
							<a href="<?= $order->getLink('field', 'coast', true); ?>"><span>Coast</span> <span class="sorting-indicator"></span></a>
						</th>
						<th>Status</th>
					</tr>
				</tfoot>
			</table>
			<div class="tablenav top">
				<div class="alignleft actions bulkactions">
					<select name="action2" id="bulk-action-selector-top">
							<option value="0">Action</option>
							<option value="delete">Delete</option>
					</select>
					<input type="submit" id="doaction" class="button action" value="Apply">
				</div>
				<br class="clear">
			</div>
		</form>
	</div>
<?php else: ?>
	<div class="wrap">
		<h1>
			You are not have orders.
			<a href="?page=booking&action=add" class="page-title-action">Add order</a>
		</h1>
	</div>
<?php endif; ?>