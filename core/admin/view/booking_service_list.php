<?php
$service = new jbServiceList();

if($_POST['add_service'] == 'Add new service')

	$service->addService($_POST);

if($_POST['add_service'] == 'Update service')
	$service->updateService($_POST, $_POST['id']);

$service_list = $service->getService();

if($_POST['action'] == 'delete' || $_POST['action2'] == 'delete')
{	
	for($i = 1; $i <= count($service_list); $i++)
	{
		if($_POST['service_'.$i])
		
		$service->deleteService($_POST['service_'.$i]);
		

	}
}

if($_POST['changeit'] == 'Edit')
{
	$action = (empty($_POST['active_action_1'])) ? $_POST['active_action_2'] :$_POST['active_action_1'];
	for ($i=0; $i <= count($service_list); $i++)
	{	
		if($_POST['service_'.$i]) 
			$service->updateServiceByAction($action, $_POST['service_'.$i]);
	}
}
$service_list = $service->getService();
?>

<div class="wrap">
	<h1>Service
		<a href="?page=booking-service&action=add" class="page-title-action">Add service</a>
	</h1>
	<form method="post" action="#">
		<div class="tablenav top">
			<div class="alignleft actions bulkactions">
				<label for="bulk-action-selector-top" class="screen-reader-text">Выберите массовое действие</label>
				<select name="action" id="bulk-action-selector-top">
					<option value="-1">Actions</option>
					<option value="delete">Delete</option>
				</select>
				<input type="submit" id="doaction" class="button action" value="Apply">
			</div>
			<div class="alignleft actions">
				<label class="screen-reader-text" for="new_role">Change category on…</label>
				<select name="active_action_1" id="active_action_1">
					<option value="">Select an action</option>			
					<option value="enable">Enable</option>
					<option value="disable">Disable</option>
				</select>
				<input type="submit" name="changeit" id="changeit" class="button" value="Edit">
			</div>
			<div class="tablenav-pages one-page">
			</div>
			<h2 class="screen-reader-text">Список пользователей</h2>
			<table class="wp-list-table widefat fixed striped users">
				<thead>
					<tr>
						<td id="cb" class="manage-column column-cb check-column">
							<label class="screen-reader-text" for="cb-select-all-1">Выделить все</label>
							<input id="cb-select-all-1" type="checkbox">
						</td>
						<th scope="col" id="username" class="manage-column column-username column-primary sortable desc">
							<span>Name of service</span>
						</th>
						<th scope="col" id="name" class="manage-column column-name">Name of category
						</th>
						<th scope="col" id="email" class="manage-column column-email sortable desc">
							<span>Coast</span>
						</th>
						<th scope="col" id="username" class="manage-column column-username column-primary sortable desc">
							<span>Active</span>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php $count = 1;
						foreach ($service_list as $key => $value):?>		
					<tr>
						<th scope="row" class="check-column">
							<input type="checkbox" name="service_<?=$count?>" id="service_<?=$count?>" class="administrator" value="<?=$value->id?>">
						</th>
						<td>
							<a href="?page=booking-service&action=add&id=<?=$value->id?>"><?=$value->name;?></a>
						</td>
						<td>
							<?=$value->category;?>
						</td>
						<td>
							<?=$value->coast;?>
						</td>
						<td>						
							<?php $check = ($value->active == 1) ? 'enable' : 'disable'; ?>
								<?=$check; ?>
						</td>
					</tr>
						<?php $count++; ?>
						<?php endforeach; ?>	
				</tbody>
				<tfoot>
					<tr>
						<td class="manage-column column-cb check-column">
							<label class="screen-reader-text" for="cb-select-all-2">Выделить все</label>
							<input id="cb-select-all-2" type="checkbox">
						</td>
						<th scope="col" class="manage-column column-username column-primary sortable desc">
							<span>Name of service</span>
						</th>
						<th scope="col" class="manage-column column-name">Name of category
						</th>
						<th scope="col" class="manage-column column-email sortable desc">
							<span>Coast</span>
						</th>
						<th scope="col" id="username" class="manage-column column-username column-primary sortable desc">
							<span>Active</span>
						</th>
					</tr>
				</tfoot>
			</table>
		<div class="tablenav bottom">
			<div class="alignleft actions bulkactions">
				<label for="bulk-action-selector-bottom" class="screen-reader-text">Выберите массовое действие</label>
				<select name="action2" id="bulk-action-selector-bottom">
					<option value="-1">Actions</option>
					<option value="delete">Delete</option>
				</select>
				<input type="submit" id="doaction2" class="button action" value="Apply">
			</div>
			<div class="alignleft actions">
				<label class="screen-reader-text" for="new_role2">Change category on…</label>
				<select name="active_action_2" id="active_action_2">
					<option value="">Select an action</option>			
					<option value="enable">Enable</option>
					<option value="disable">Disable</option>
				</select>
				<input type="submit" name="changeit" id="changeit" class="button" value="Edit">
			</div>
			<div class="tablenav-pages one-page">
				<span class="displaying-num">1 элемент</span>
				<span class="pagination-links">
					<span class="tablenav-pages-navspan" aria-hidden="true">«</span>
					<span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
					<span class="screen-reader-text">Текущая страница</span>
					<span id="table-paging" class="paging-input">
						<span class="tablenav-paging-text">1 из 
							<span class="total-pages">1</span>
						</span>
					</span>
					<span class="tablenav-pages-navspan" aria-hidden="true">›</span>
					<span class="tablenav-pages-navspan" aria-hidden="true">»</span>
				</span>
			</div>
			<br class="clear">
		</div>
	</form>
	<br class="clear">
</div>