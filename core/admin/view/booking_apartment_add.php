<?php

$category = new jbCategory();
$apartment = new jbApartment();

$category_list = $category->getCategory('enable');

$submit = 'Add new apartment';

$name_apartment = '';

$checking = 1;

if($_GET['id'])
{
	$apartment_info = $apartment->getApartmentById($_GET['id']);
	$name_apartment = $apartment_info->name;
	$checking = $apartment_info->active;
	$submit = 'Update apartment';
}
?>

<div class="wrap">
	<h1 id="add-new-user">Add new apartment</h1>


	<div id="ajax-response"></div>

	<p>Create a new service and add booking system.</p>
	<form method="post" name="createuser" id="createuser" class="validate" novalidate="novalidate" action="?page=booking-apartment">

		<table class="form-table">
			<tbody>
				<tr class="form-field form-required">
					<th scope="row">
						<label for="name_service">Name apartment 
							<span class="description">(required)
							</span>
						</label>
					</th>
					<td>
						<input name="name apartment" type="text" id="name apartment" value="<?=$name_apartment?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
						<p class="description" id="tagline-description">Name of apartment Booking System</p>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row">
						<label for="Category">Category</label>
					</th>
					<td>
						<select name="category" id="category">
							<?php foreach ($category_list as $key => $value):?>
								
									<option value="<?=$value->name?>"><?=$value->name?></option>
								
							<?php endforeach; ?>			
						</select>
						<p class="description" id="tagline-description">Select a category for this apartment</p>
					</td>
				</tr>
				<tr>
				<th scope="row">Apartment active</th>
					<td>
						<input type="checkbox" name="active" id="active" value="1"''
						<?php	$check = ($checking == 1) ? 'checked' : ''; 
							echo ''.$check.'>';
						?>
						<p class="description" id="tagline-description">Enable or Disable Apartment</p>
					</td>
				</tr>
			</tbody>
		</table>
		<p class="submit">
		<input type="submit" name="add_apartment" id="add_apartment" class="button button-primary" value="<?=$submit?>">
		</p>
		<input type="hidden" name="id" value="<?=$_GET['id']?>">
	</form>
</div>';