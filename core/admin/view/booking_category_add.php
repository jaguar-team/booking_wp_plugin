<?php
$category = new jbCategory();

$category_name = '';

$button = 'Save Changes';

if($_GET['id'])
{
    $result = $category->getCategoryById($_GET['id']);

    $category_name = $result->name;

    $button = 'Update category';
}


?>



<div class ="wrap">
	<h1>Category booking</h1>
	<form method="post" enctype="multipart/form-data" action="?page=booking-category">
	   <table width="100%" class="form-table">
            <tbody>
                <tr>
                    <th align="right" scope="row">Category name</th>
                    <td align="center"></td>
                    <td>
                        <input name="name_category" type="text" id="booking_button_text" value="<?=$category_name?>">
                        <p class="description" id="tagline-description">Category name the booking service</p>
                        <?=nbsp?><a href="#" rel="tooltip" title="Booking Button Text">
                                    <i class="icon-question-sign"></i>
                                    </a>
                    </td>
                </tr>

                <tr>
                    <th align="right" scope="row">Category active</th>
                    <td align="center"><strong></strong></td>
                    <td>
                        <input type="checkbox" name="active_category" id="active_category" value="1" >
                        <p class="description" id="tagline-description">Enable or Disable Category</p>
                    </td>
                </tr>

            </tbody>
        </table>
		<p class="submit">  
			<input type="submit" class="button-primary" value="<?=$button?>" name="save"/>
            <input type="hidden" name="id" value="<?=$_GET['id']?>">  
		</p>
	</form>
</div>
