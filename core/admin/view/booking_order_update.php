<?php 

$order = new jbOrderList(); 

$status = $order->getStatus();

?>
<div class="wrap">
	<h1>Update order</h1>
	<p>Update order of the booking system.</p>
	<form action="?page=booking" method="post">
		<input type="hidden" name="order_add[coast]" value="743.00" />
		<table class="form-table">
			<tbody>
				<tr class="form-field">
					<th scope="row"><label for="user_login">Service</label></th>
					<td>
						<select name="order_add[service]">
							<option selected="selected" value="Bed & Breakfast">Bed & Breakfast</option>
							<option value="Self Catering">Self Catering</option>		
						</select>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row"><label for="user_login">Self</label></th>
					<td>
						<select name="order_add[self]">
							<option selected="selected" value="Granary Cottage">Granary Cottage</option>
							<option value="The Nest">The Nest</option>
							<option value="Stable Cottage">Stable Cottage</option>
							<option value="Hazel Cottage">Hazel Cottage</option>	
						</select>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row"><label for="input_check_in">Check in <span class="description">(required)</span></label></th>
					<td><input id="input_check_in" name="order_add[check_in]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th scope="row"><label for="input_check_out">Check out <span class="description">(required)</span></label></th>
					<td><input id="input_check_out" name="order_add[check_out]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label for="user_login">Pets</label></th>
					<td>
						<label>
							<input type="checkbox" name="order_add[pets]" value="1">
						</label>
					</td>
				</tr>
				<tr class="form-field">
					<th scope="row"><label for="user_login">Status</label></th>
					<td>
						<select name="order_add[status]">
							<?php foreach ($status as $key => $value): ?>
								<option style="color: <?= $value['color'] ?>" <?= ($value['active'] ? 'selected="selected"' : ''); ?> value="<?= $key ?>"><?= $value['name'] ?></option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<h1>Info about customer</h1>
		<table class="form-table">
			<tbody>
				<tr class="form-field">
					<th><label for="">First name <span class="description">(required)</span></label></th>
					<td><input  name="user_add[first_name]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Last name <span class="description">(required)</span></label></th>
					<td><input name="user_add[last_name]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Email <span class="description">(required)</span></label></th>
					<td><input name="user_add[email]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Phone number <span class="description">(required)</span></label></th>
					<td><input name="user_add[phone_number]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Mobile number <span class="description">(required)</span></label></th>
					<td><input name="user_add[mobile_number]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Adress <span class="description">(required)</span></label></th>
					<td><input name="user_add[adress]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">City <span class="description">(required)</span></label></th>
					<td><input name="user_add[city]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">State <span class="description">(required)</span></label></th>
					<td><input name="user_add[state]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Post code <span class="description">(required)</span></label></th>
					<td><input name="user_add[post]" type="text" class="regular-text code"></td>
				</tr>
				<tr class="form-field">
					<th><label for="">Country <span class="description">(required)</span></label></th>
					<td><input name="user_add[country]" type="text" class="regular-text code"></td>
				</tr>
			</tbody>
		</table>
		<div class="submit" style="clear:both;">
			<input type="submit" name="createuser" id="createusersub" class="button button-primary" value="Add new order">
		</div>
	</form>
</div>
