<?php
$category = new jbCategory();

$category_list = $category->getCategory();

if($_POST['save'] == 'Update category')
    $category->updateCategory($_POST['name_category'], $_POST['id'], $_POST['active_category']);

if($_POST['action_1'] == 'delete' || $_POST['action_2'] == 'delete' )
{	
	for($i = 1; $i <= count($category_list); $i++) 
	{
		if($_POST['category_'.$i])
		{
			$category->deleteCategoryById($_POST['category_'.$i]);
			
		}
	}
}
if($_POST['save'] == 'Save Changes')
   $category->addCategory($_POST['name_category'], $_POST['active_category']);

if($_POST['changeit'] == 'Edit')
{
	$action = (empty($_POST['active_action_1'])) ? $_POST['active_action_2'] :$_POST['active_action_1'];
	for ($i=1; $i <= count($category_list); $i++)
	{	
		if($_POST['category_'.$i]) 
			$category->updateCategoryByAction($action, $_POST['category_'.$i]);
		
			
	}
}
$category_list = $category->getCategory();
?>

<div class="wrap">
	<h1>Accommodation
		<a href="?page=booking-category&action=add" class="page-title-action">Add accommodation
		</a>
	</h1>
	<form method="post" action="#">
		<div class="tablenav top">
		<div class="alignleft actions bulkactions">
		<label for="bulk-action-selector-top" class="screen-reader-text">Выберите массовое действие</label>
		<select name="action_1" id="bulk-action-selector-top">
			<option value="-1">Actions</option>
			<option value="delete">Delete</option>
		</select>
		<input type="submit" id="doaction" class="button action" value="Apply">
		</div>
		<div class="alignleft actions">				
			<select name="active_action_1" id="active_action_1">
				<option value="">Select an action</option>			
				<option value="enable">Enable</option>
				<option value="disable">Disable</option>
			</select>
			<input type="submit" name="changeit" id="changeit" class="button" value="Edit">
		</div>
		<div class="tablenav-pages one-page">
			<span class="displaying-num">1 элемент</span>
			<br class="clear">
		</div>	
		<h2 class="screen-reader-text">Список пользователей</h2>
			<table class="wp-list-table widefat fixed striped users">
				<thead>
					<tr>
						<td id="cb" class="manage-column column-cb check-column">
							<label class="screen-reader-text" for="cb-select-all-1">Выделить все</label>
							<input id="cb-select-all-1" type="checkbox">
						</td>
						<th scope="col" id="username" class="manage-column column-username column-primary sortable desc">
							<span>Name of service</span>
						</th>
						<th scope="col" id="username" class="manage-column column-username column-primary sortable desc">
							<span>Active</span>
						</th>
					</tr>
				</thead>
				<tbody>	
					<?php $count = 1;
						foreach ($category_list as $key => $value): ?>		
					<tr>
						<th scope="row" class="check-column">
							<input type="checkbox" name="category_<?=$count?>" id="category_<?=$count?>" class="administrator" value="<?=$value->id?>">
						</th>
						<td>
							<a href="?page=booking-category&action=add&id=<?=$value->id?>"><?=$value->name;?></a>
						</td>
						<td>
							<?php $check = ($value->active == 1) ? 'enable' : 'disable'; ?>
							<?=$check; ?>
						</td>				
					</tr>	
						<?php $count++; ?>
						<?php endforeach; ?>	
				</tbody>
				<tfoot>
				<tr>
					<td class="manage-column column-cb check-column">
						<label class="screen-reader-text" for="cb-select-all-2">Выделить все</label>
						<input id="cb-select-all-2" type="checkbox">
					</td>
					<th scope="col" class="manage-column column-username column-primary sortable desc">
						<span>Name of category</span>
					</th>
					<th scope="col" id="username" class="manage-column column-username column-primary sortable desc">
						<span>Active</span>
					</th>
				</tr>
				</tfoot>
			</table>
		<div class="tablenav bottom">
			<div class="alignleft actions bulkactions">
				<label for="bulk-action-selector-bottom" class="screen-reader-text">Выберите массовое действие</label>
				<select name="action_2" id="bulk-action-selector-bottom">
					<option value="-1">Actions</option>
					<option value="delete">Delete</option>
				</select>
				<input type="submit" id="doaction2" class="button action" value="Apply">
			</div>
			<div class="alignleft actions">
				<select name="active_action_2" id="active_action_2">
					<option value="">Select an action</option>
					<option value="enable">Enable</option>
					<option value="disable">Disable</option>
				</select>
				<input type="submit" name="changeit" id="changeit" class="button" value="Edit">
			</div>
			<div class="tablenav-pages one-page">
				<span class="displaying-num">1 элемент</span>
				<br class="clear">
			</div>
		</div>	

</form>

<br class="clear">
</div>
