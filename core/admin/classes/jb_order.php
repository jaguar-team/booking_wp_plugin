<?php

class jbOrderList extends Controller {

	/**
	 * @var string table name
	 */
	public $table_name = 'jb_order';

	/**
	 * @var string name array post method
	 */
	public $name_post = 'order_add';


	/**
	 * @var array name fileds post data
	 */
	public $fields  = array(
		'id' 		=> ['order' => 'desc'],
		'apartment'	=> [],
		'service' 	=> [],
		'self' 		=> [],
		'check_in' 	=> ['order' => 'desc'],
		'check_out' => ['order' => 'desc'],
		'pets' 		=> ['default' => 0],
		'coast' 	=> ['order' => 'desc'],
		'status' 	=> ['order' => 'desc'],
	);

	public static $post_name = 'order_add';

	public static $definition  = array(
		'service' 	=> ['validation' => 'is_email', 'type' => '%s', 'required' => true],
		'apartment' => ['validation' => 'is_email', 'type' => '%s', 'required' => true],
		'self' 		=> ['validation' => 'is_email', 'type' => '%s', 'required' => true],
		'check_in' 	=> ['validation' => 'is_email', 'type' => '%s', 'required' => true],
		'check_out' => ['validation' => 'is_email', 'type' => '%s', 'required' => true],
		'pets' 		=> ['validation' => 'is_email', 'type' => '%s', 'required' => true],
		'coast' 	=> ['validation' => 'is_email', 'type' => '%f', 'required' => true],
		'status' 	=> ['validation' => 'is_email', 'type' => '%d', 'required' => true],
	);

	/**
	 * @var int id now inster/update order
	 */
	private $now_id;

	/**
	 * @var array list status order
	 */
	private $status = array(
		1 => ['name' => 'Confirmed', 'color' => 'green'],
		2 => ['name' => 'Not confirmed', 'color' => 'red', 'active' => true],
	);

	/**
	 * @var array list pets
	 */
	private $pets = array(
		0 => ['name' => 'No', 'color' => 'green', 'active' => true],
		1 => ['name' => 'Yes', 'color' => 'green'],
	);

	public function getSortOrder($where = false, $order_by = false, $count = false)
	{
		global $wpdb;

		$sql = 'SELECT * FROM '.$this->getTableName().' WHERE `check_in` >= DATE_FORMAT(NOW(),"%Y-%m-%d")';

		if ($where)
			$sql .= ' AND '.$where['field'].'="'.$where['value'].'"';
		if ($order_by)
			$sql .= ' ORDER BY '.$order_by['field'].' '.$order_by['value'];

		return ($count ? count($wpdb->get_results($sql)) : $wpdb->get_results($sql));
	}

	/** public function getOrder($field ='id', $order_by = 'asc')
	{
		global $wpdb;

		return $wpdb->get_results('SELECT * FROM '.$this->getTableName().' ORDER BY '.$field.' '.$order_by);
	} **/

	/** public function getOrderByFieldValue($field, $value, $count = false)
	{
		global $wpdb;

		$result = $wpdb->get_results('SELECT * FROM '.$this->getTableName().' WHERE '.$field.'="'.$value.'"');

		if ($count)
			return count($result);

		return $result;
	} **/

	public function getById($id)
	{
		global $wpdb;

		return $order_list = $wpdb->get_row('SELECT * FROM '.$this->getTableName().' WHERE id="'.$id.'"');
	}

	public function addOrder()
	{
		global $wpdb;

		$data = $_POST[$this->name_post];

		$format = array('%s', '%s', '%s', '%s', '%s', '%s', '%f', '%d');

		if (!$wpdb->insert($this->getTableName(), $data, $format))
			return false;

		return $this->now_id = $wpdb->insert_id;
	}

	public function delete($id_orders)
	{
		global $wpdb;

		if ($id_orders)
			foreach ($id_orders as $key => $value) {
				$wpdb->delete($this->getTableName(), array('id' => $value));
			}

		return true;
	}

	public function update($id)
	{
		global $wpdb;

		$data = $_POST[$this->name_post];

		// $format = array('%s', '%s', '%s', '%s', '%s', '%f', '%d');

		$where  = array('id' => $id);

		return $wpdb->update($this->getTableName(), $data, $where, $format);
	}

	public function getDate($json = true)
	{
		global $wpdb;

		$order_list = $this->getSortOrder();

		$array_date = array();

		foreach ($order_list as $key => $value) {
			if ($array_date[$value->service][$value->apartment])
				array_push($array_date[$value->service][$value->apartment], array('start' => $value->check_in, 'finish' => $value->check_out));
			else
				$array_date[$value->service][$value->apartment]  = array(array('start' => $value->check_in, 'finish' => $value->check_out));
		}

		return ($json ? json_encode($array_date) : $array_date);
	}

	/**
	 * Get table name
	 *
	 * @return string
	 */
	public function getTableName()
	{
		global $wpdb;

		return $wpdb->prefix.$this->table_name;
	}

	/**
	 * Get list status order
	 *
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Get list pets order
	 *
	 * @return string
	 */
	public function getPets()
	{
		return $this->pets;
	}

	/**
	 * Get now id insert order
	 *
	 * @return string
	 */
	public function getId()
	{
		return $this->now_id;
	}

	public function test()
	{
		global $_POST;

		parent::add();

		return true;
	}
}