<?php

class jbUser {

	/**
	 * @var string table name
	 */
	public $table_name = 'jb_user';

	/**
	 * @var string name array post method
	 */
	public $name_post = 'user_add';

	/**
	 * Get user by id
	 *
	 * @return bool
	 */
	public function getUserByOrderId($id)
	{
		global $wpdb;

		return $order_list = $wpdb->get_row('SELECT * FROM '.$this->getTableName().' WHERE id_order="'.$id.'"');
	}

	/**
	 * Add user
	 *
	 * @return bool
	 */
	public function add($id_order)
	{
		global $wpdb;

		$data = $_POST[$this->name_post];
		$data['id_order'] = $id_order;

		// $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d');

		return $wpdb->insert($this->getTableName(), $data, $format); 
	}

	/**
	 * Update user data
	 *
	 * @return bool
	 */
	public function update($id_order)
	{
		global $wpdb;

		$data = $_POST[$this->name_post];

		// $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d');

		$where  = array('id_order' => $id_order);

		return $wpdb->update($this->getTableName(), $data, $where, $format);
	}

	/**
	 * Delete user
	 *
	 * @return bool
	 */
	public function delete()
	{

	}

	/**
	 * Get table name
	 *
	 * @return string
	 */
	public function getTableName()
	{
		global $wpdb;

		return $wpdb->prefix.$this->table_name;
	}
}