<?php
class jbCategory {

	public function getCategory($action = '')
	{
		global $wpdb;
		if(empty($action))
		return $category_list = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_category');
		else if($action == 'enable')
			{	
				$active = 1;
				return $category_active = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_category WHERE active = '.$active);
			}	
		else
			$active = 0;
				return $category_active = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_category WHERE active = '.$active);
	}

	
	public function addCategory($name, $active)
	{
		global $wpdb;
		if(empty($active))
			$active = 0;

		$table_name = $wpdb->prefix.'jb_category';

		if(empty($wpdb->get_results('SELECT * FROM '.$table_name.' WHERE name = "'.$name.'"')))
			{
				return $wpdb->insert($table_name, 
					array('name' 	=> $name,
						  'active'	=> $active),
					array('%s', '%d'));

			}
		else
			return false;
	}

	public function getCategoryById($id)
	{
		global $wpdb;

		$table_name = $wpdb->prefix.'jb_category';

		return $wpdb->get_row('SELECT * FROM '.$table_name.' WHERE id = '.$id);
	}


	public function updateCategory($name, $id, $active)
	{
		global $wpdb;

		if(empty($active))
			$active = 0;
		$table_name = $wpdb->prefix.'jb_category';

		if(empty($wpdb->get_results('SELECT * FROM '.$table_name.' WHERE name = "'.$name.'"')))
		{
			return $wpdb->update($table_name, 				
				array('name' => $name,
					  'active' => $active),
				array('id' 	 => $id));
		}
		else
			return $wpdb->update($table_name,
				array('active' => $active),
				array('id'	   => $id));

	}

	public function deleteCategoryById($id)
	{
		global $wpdb;

		$table_name = $wpdb->prefix.'jb_category';

		return $wpdb->delete($table_name, 
			array('id' => $id));
	}


	public function updateCategoryByAction($action, $id)
	{
		global $wpdb;

		$table_name = $wpdb->prefix.'jb_category';

		if($action == 'enable')
			return $wpdb->update($table_name, 
				array('active' => 1),
				array('id'     => $id));
		else if($action == 'disable'){
			return $wpdb->update($table_name, 
				array('active' => 0),
				array('id'     => $id));
		}
		else
			return false;
	}


}