<?php
class jbApartment{

	public function getApartment($action = '')
	{
		global $wpdb;
		if(empty($action))
		return $category_list = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_apartment');
		else if($action == 'enable')
			{	
				$active = 1;
				return $category_active = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_category WHERE active = '.$active);
			}	
		else
			$active = 0;
				return $category_active = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_category WHERE active = '.$active);
	}

	public function addApartment($data)
	{
		global $wpdb;
		$table_name = $wpdb->prefix.'jb_apartment';
		if(empty($data['active']))
			$data['active'] = 0;
		if(empty($wpdb->get_results('SELECT * FROM '.$table_name.' WHERE name = "'.$data['name_apartment'].'"')))
			{
				return $wpdb->insert($table_name,
					 array('name' 		=> $data['name_apartment'], 
						  'category' 	=> $data['category'],
						  'active'		=> $data['active']),
					  array('%s', '%s', '%d'));
			}
		else
			return false;
	}

	public function getApartmentById($id)
	{
		global $wpdb;

		$table_name = $wpdb->prefix.'jb_apartment';

		return $wpdb->get_row('SELECT * FROM '.$table_name.' WHERE id = '.$id);
	}

	public function getApartamentByCategory($category)
	{
		global $wpdb;

		$table_name = $wpdb->prefix.'jb_apartment';
		
		return $service_by_category = $wpdb->get_results('SELECT * FROM '.$table_name.' WHERE category = "'.$category.'"');
	}

		public function updateApartment($data)
	{
		global $wpdb;

		$table_name = $wpdb->prefix.'jb_apartment';

		if(empty($data['active']))
			$data['active'] = 0;
		if(empty($wpdb->get_results('SELECT * FROM '.$table_name.' WHERE name = "'.$data['name_apartment'].'"')))
		{
		return $wpdb->update($table_name, 
			array('name' 		=> $data['name_apartment'],
				  'category'	=> $data['category'],
				  'active'		=> $data['active'],),
			array('id' 			=> $data['id']),
			array('%s', '%s', '%d'));
		}
		else return $wpdb->update($table_name, 
			array('category'	=> $data['category'],
				  'active'		=> $data['active'],),
			array('id' 			=> $data['id']),
			array('%s', '%d'));



	}

		public function updateApartmentByAction($action, $id)
			{
				global $wpdb;

				$table_name = $wpdb->prefix.'jb_apartment';
				if($action == 'enable')
					return $wpdb->update($table_name, 
						array('active' => 1),
						array('id'     => $id));
				else if($action == 'disable'){
					return $wpdb->update($table_name, 
						array('active' => 0),
						array('id'     => $id));
				}
				else
					return false;

			}
		public function deleteApartment($id)
			{

				global $wpdb;

				$table_name = $wpdb->prefix.'jb_apartment';
				
				return $wpdb->delete($table_name, 
					array('id' => $id));
			}


}