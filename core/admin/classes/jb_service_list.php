<?php

class jbServiceList {

	public $default_service_B = array('0' => ['name' => 'Single', 		  'coast' => 77],
									  '1' => ['name' => 'Double', 		  'coast' => 97],
									  '2' => ['name' => 'Family', 		  'coast' => 117],
									  );
	public $default_service_S = array('0' => ['name' => '3 nights(check in on Friday)', 		  'coast' => 250],
									  '1' => ['name' => '4 nights(check in on Thusday)', 		  'coast' => 320],
									  '2' => ['name' => '7 nights(check in on Monday)', 		  'coast' => 630],
									  );

	public function getService($action = '')
	{
		global $wpdb;
		if(empty($action))
			return $order_list = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_service');
		else if($action == 'enable')
			{	
				$active = 1;
				return $category_active = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_service WHERE active = '.$active);
			}	
		else
			$active = 0;
				return $category_active = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_service WHERE active = '.$active);
	}

	public function addService($data)
	{
		global $wpdb;
		$table_name = $wpdb->prefix.'jb_service';
		if(empty($data['active']))
			$data['active'] = 0;
		if(empty($wpdb->get_results('SELECT * FROM '.$table_name.' WHERE name = "'.$data['name_service'].'"')))
			{
				$wpdb->insert($table_name,
					 array('name' 		=> $data['name_service'], 
						  'category' 	=> $data['category'],
						  'coast'	 	=> doubleval($data['coast']),
					 	  'active'		=> $data['active']),
					  array('%s', '%s', '%f', '%d'));
			}
		else
			return false;
		
		
			
		
	}

	public function updateService($data, $id)
	{
		global $wpdb;

		$table_name = $wpdb->prefix.'jb_service';

		if(empty($data['active']))
			$data['active'] = 0;
		if(empty($wpdb->get_results('SELECT * FROM '.$table_name.' WHERE name = "'.$data['name_service'].'"')))
		{
		return $wpdb->update($table_name, 
			array('name' 		=> $data['name_service'],
				  'category'	=> $data['category'],
				  'coast'		=> $data['coast'],
				  'active'		=> $data['active'],),
			array('id' 			=> $id),
			array('%s', '%s', '%d', '%d'));
		}
		else $wpdb->update($table_name, 
			array('category'	=> $data['category'],
				  'active'		=> $data['active'],),
			array('id' 			=> $id),
			array('%s', '%d'));



	}

	public function updateServiceByAction($action, $id)
	{
		global $wpdb;

		$table_name = $wpdb->prefix.'jb_service';
		if($action == 'enable')
			return $wpdb->update($table_name, 
				array('active' => 1),
				array('id'     => $id));
		else if($action == 'disable'){
			return $wpdb->update($table_name, 
				array('active' => 0),
				array('id'     => $id));
		}
		else
			return false;

	}

	public function deleteService($id)
	{

		global $wpdb;

		$table_name = $wpdb->prefix.'jb_service';
		
		return $wpdb->delete($table_name, 
			array('id' => $id));
	}

	public function getServiceById($id)
	{
		global $wpdb;

		return $info_service = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'jb_service WHERE id = '.$id);
	}

	public function getServiceByCategory($category)
	{
		global $wpdb;

		return $service_by_category = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'jb_service WHERE category = "'.$category.'"');
	}
}

?>