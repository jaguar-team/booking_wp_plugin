<?php

class Controller {

	public static $definition;

	public static $post_name;

	public $field_post;

	public $field_errors = array();

	public function __construct()
	{
		$this->field_post = array(
			'apartment' => 'andreyd37@gmail.com',
			'service' 	=> 'service',
			'self' 		=> 'selffff',
			'check_in' 	=> date('Y-m-d'),
			'check_out' => date('Y-m-d'),
			'pets' 		=> 1,
			'coast' 	=> 245.00,
			'status' 	=> 1,
		);

		if ($this->init())
			$this->validate();
	}

	public function init()
	{
		/** init post name **/
		if (!isset($this::$post_name))
			return false;

		/** init $definition **/
		if (!isset($this::$definition))
			return false;

		/** init post data field **/
		/* if (!isset($_POST[$this->name_post]))
			return false;
		$this->field_post = $_POST[$this->name_post]; */
		return true;
	}

	public function validate()
	{
		foreach ($this->field_post as $key => $value) {
			
			// Check required
			if (isset($this::$definition[$key]['required']))
				if ($this::$definition[$key]['required'])
					if (empty($value))
						array_push($this->field_errors, 'Field '.$key.' is required.');

			// Check size 
			if (isset($this::$definition[$key]['size'])) {

				$size = $this::$definition[$key]['size'];
				$length = strlen($value);

				// Check min size
				if (isset($size['min']) && $length < $size['min'])
					array_push($this->field_errors, 'Field '.$key.' must contain more '.$size['min'].' symbols.');
				// Check max size
				if (isset($size['max']) && $length > $size['max'])
					array_push($this->field_errors, 'Field '.$key.' must contain not more '.$size['max'].' symbols.');
			}

			// Check field validator
			if (isset($this::$definition[$key]['validation']))
				if (!call_user_func($this::$definition[$key]['validation'], $value))
						array_push($this->field_errors, 'Field '.$key.' not valid');


		}

		//var_dump($this->field_errors);
	}

	public static function add()
	{
		return true;
	}

	public static function getCurrentSort($field)
	{
		parse_str($_SERVER['QUERY_STRING'], $vars);

		return (($vars['field'] == $field) ? $vars['order'] : 'asc');
	}

	public function getLink($field = false, $value = false, $order = false)
	{
		parse_str($_SERVER['QUERY_STRING'], $vars);

		if ($field && $value)
			$vars[$field] = $value;
		else
			unset($vars['status']);

		if ($order)
			$vars['order'] = (($vars['order'] == 'asc') ? 'desc' : 'asc');

		return '?'.http_build_query($vars);
	}

	public function checkActiveLink($field, $value = false)
	{
		parse_str($_SERVER['QUERY_STRING'], $vars);

		if ($field && !$value && !$vars[$field])
			return true;

		return ($vars[$field] == $value ? true : false);
	}
}