<?php

	$action = $_GET['action'];
	$order = new jbOrderList();
	$user = new jbUser();
	$accomodation = new jbCategory();
	$service = new jbServiceList();
	$appartment = new jbApartment();

	switch ($action) {
		case 'add':
			require_once('view/booking_order_add.php');
			break;
		case 'update':
			global $order_data, $user_data;

			$id = $_GET['id'];

			$order_data = $order->getByid($id);
			$user_data = $user->getUserByOrderId($id);

			require_once('view/booking_order_add.php');
			break;
		default:

			if (!empty($_POST[$order->name_post]) && !empty($_POST[$user->name_post])) {
				switch ($_POST['action_input']) {
					case 'update':
						if ($order->update($_POST['id']) || $user->update($_POST['id']))
							$jb_message = 'Order updated.';
						break;
					case 'add':
						if ($order->addOrder() && $user->add($order->getId()))
							$jb_message = 'New order created.';
						break;
					default:
						# code...
						break;
				}
			}

			if ($_POST['action'])
				$action_input = $_POST['action'];

			if ($_POST['action2'])
				$action_input = $_POST['action2'];
				
			switch ($action_input) {
				case 'delete':
					$order->delete($_POST['order']);
					break;	
				default:
					break;
			}

			require_once('view/booking_order_list.php');
			break;
	}