<?php
/*
   Plugin Name: Booking
   Plugin URI: http://wordpress.org/extend/plugins/booking/
   Version: 0.1
   Author: Jaguar-team
   Description: Booking system
   Text Domain: booking
   License: GPLv3
  */

/*
    "WordPress Plugin Template" Copyright (C) 2016 Michael Simpson  (email : michael.d.simpson@gmail.com)

    This following part of this file is part of WordPress Plugin Template for WordPress.

    WordPress Plugin Template is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordPress Plugin Template is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Contact Form to Database Extension.
    If not, see http://www.gnu.org/licenses/gpl-3.0.html
*/

$file = __FILE__;

/** install **/

require_once("core/install.php");

if (is_admin())
    register_back_css_js();
else
    register_front_css_js();

/** classes **/
require_once("core/admin/booking_controller.php");
require_once("core/admin/classes/jb_order.php");
require_once("core/admin/classes/jb_user.php");
require_once("core/admin/classes/jb_service_list.php");
require_once("core/admin/classes/jb_setting.php");
require_once("core/admin/classes/jb_category.php");
require_once("core/admin/classes/jb_apartment.php");

/** registration admin menu **/
add_action('admin_menu','booking_admin_menu');

function booking_admin_menu() {
    $menu   = add_menu_page('Booking', 'Booking', 'administrator', 'booking', 'display_booking_order_list_menu');
    $sub_menu_1 = add_submenu_page('booking', 'Accommodation', 'Accommodation', 'administrator', 'booking-category', 'display_booking_category_menu');
    $sub_menu_2 = add_submenu_page('booking', 'Service', 'Service', 'administrator', 'booking-service', 'display_booking_service_menu');
    $sub_menu_3 = add_submenu_page('booking', 'Setting', 'Setting', 'administrator', 'booking-setting', 'display_booking_setting_menu');
    $sub_menu_4 = add_submenu_page('booking', 'Apartment', 'Apartment', 'administrator', 'booking-apartment', 'display_booking_apartment_menu');
}

function display_booking_order_list_menu() {
    require_once("core/admin/booking_order_list.php");
}
function display_booking_service_menu() {
    require_once("core/admin/booking_service.php");
}
function display_booking_setting_menu() {
    require_once("core/admin/booking_setting.php");
}
function display_booking_category_menu() {
    require_once("core/admin/booking_category.php");
}
function display_booking_apartment_menu() {
    require_once("core/admin/booking_apartment.php");
}

/** registration shortcode **/
add_shortcode('JAGUAR_BOKKING', 'display_booking_shortcode');

function display_booking_shortcode() {
    require_once("core/booking_display.php");
}


function register_front_css_js() {
    wp_register_style('bootstrap',plugins_url('/css/bootstrap.min.css', __FILE__));
    wp_enqueue_style('bootstrap');

    wp_register_style('style',plugins_url('/css/style.css', __FILE__));
    wp_enqueue_style('style');

    wp_register_style('jquery',plugins_url('/css/jquery-ui.min.css', __FILE__));
    wp_enqueue_style('jquery');

    wp_register_style('the-modal',plugins_url('/css/the-modal.css', __FILE__));
    wp_enqueue_style('the-modal');    
    
    
    wp_enqueue_script('jquery-ui',plugins_url('/js/jquery-ui.min.js', __FILE__),array('jquery'));

    wp_enqueue_script('booking',plugins_url('/js/booking.js', __FILE__),array('jquery'));  

    wp_enqueue_script('jquery-modal',plugins_url('/js/jquery.the-modal.js', __FILE__),array('jquery'));

    /* 
    wp_enqueue_script('tooltip',plugins_url('/menu-pages/bootstrap-assets/js/bootstrap-tooltip.js', __FILE__),array('jquery')); 
    */
}

function register_back_css_js() {

    wp_register_style('jquery-ui-css',plugins_url('/css/jquery-ui.min.css', __FILE__));
    wp_enqueue_style('jquery-ui-css');

    wp_enqueue_script(array('jquery', 'jquery-ui-datepicker'));
    wp_enqueue_script('jb-admin',plugins_url('/js/admin/jb_admin.js', __FILE__));
}
